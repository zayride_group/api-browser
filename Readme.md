## API Browser - Zettablock

### What is it
This app allows you to browse and manage API docs from public endpoint https://62a6bb9697b6156bff7e6251.mockapi.io/v1/apis

### Technology

1. Vite
2. React
3. Typescript
3. Styled Components

### Where can I access

Here's a [public url](https://api-browser.vercel.app/) for the app, deployed on Vercel.