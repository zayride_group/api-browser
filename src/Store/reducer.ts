import Action from "../Interface/Action";
import APIEndpointType from "../Interface/API";
import DataActions from "../Interface/DataActions";
import PendingAction from "../Interface/PendingAction";
import Sorts from "../Interface/Sorts";
import Store from "../Interface/Store";
import Actions from "./Actions";

const persistPendingActions = async (pendingActions: PendingAction[]) => {
  const persistActionPromises = pendingActions.map(async (action) => {
    return await fetch(
      `${
        import.meta.env.API_URL ||
        "https://62a6bb9697b6156bff7e6251.mockapi.io/v1"
      }/apis/${action.item.id}`,
      {
        method: action.type === DataActions.DELETE ? "DELETE" : "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        ...(action.type === DataActions.UPDATE
          ? {
              body: JSON.stringify({
                description: action.newItem?.description,
              }),
            }
          : {}),
      }
    );
  });

  await Promise.allSettled(persistActionPromises);
};

const reducer = (state: Store, action: Action): Store => {
  switch (action.type) {
    case Actions.FETCHING_DATA:
      return { ...state, fetchingData: action.payload as boolean };
    case Actions.CHANGE_CURRENT_PAGE:
      return { ...state, currentPage: action.payload as number };
    case Actions.CHANGE_CURRENT_ACTIVE_ITEM:
      return { ...state, activeItem: action.payload as APIEndpointType };
    case Actions.CHANGE_CURRENT_UPDATE_ITEM:
      return { ...state, updateItem: action.payload as APIEndpointType };
    case Actions.UPDATE_DATA:
      return { ...state, data: action.payload as APIEndpointType[] };
    case Actions.UPDATE_FILTERED_DATA:
      return { ...state, filteredData: action.payload as APIEndpointType[] };
    case Actions.UPDATE_ACTIVE_SORT:
      return { ...state, activeSort: action.payload as Sorts };
    case Actions.CREATE_ITEM_POPUP:
      return { ...state, createNewItem: action.payload as boolean };
    case Actions.ADD_PENDING_ACTION:
      return {
        ...state,
        pendingActions: [
          ...state.pendingActions,
          action.payload as PendingAction,
        ],
      };
    case Actions.REMOVE_PENDING_ACTION:
      const payload = action.payload as PendingAction;

      const newPendingActions = state.pendingActions.flatMap(
        (pendingAction) => {
          return pendingAction.item.id === payload?.item.id
            ? []
            : [pendingAction];
        }
      );

      return { ...state, pendingActions: newPendingActions };

    case Actions.PERSIST_PENDING_ACTION:
      persistPendingActions(state.pendingActions);
      return state;
    default:
      return state;
  }
};
export default reducer;
