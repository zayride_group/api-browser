import React, { Dispatch } from "react";
import Store from "../Interface/Store";
import InitialState from "./InitialState";

const AppContext = React.createContext<{
  state: Store;
  dispatch: Dispatch<{ type: string; payload: unknown }>;
}>({
  state: InitialState,
  dispatch: () => null,
});

export default AppContext;
