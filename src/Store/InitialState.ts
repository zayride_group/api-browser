import Sorts from "../Interface/Sorts";
import Store from "../Interface/Store";

const InitialState: Store = {
  currentPage: 1,
  activeItem: null,
  updateItem: null,
  data: [],
  filteredData: [],
  activeSort: Sorts.DEFAULT,
  pendingActions: [],
  createNewItem: false,
  fetchingData: false,
};

export default InitialState;
