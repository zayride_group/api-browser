import React, { useContext, useEffect, useState } from "react";
import DataBrowser from "../../Components/DataBrowser";
import Paginator from "../../Components/Paginator";
import { StyledHome } from "../../Components/Styled/Home";
import StyledForm from "../../Components/Styled/Home/StyledForm";
import StyledHeader from "../../Components/Styled/Home/StyledHeader";
import StyledRow from "../../Components/Styled/Home/StyledRow";
import StyledButton from "../../Components/Styled/StyledButton";
import APIEndpointType from "../../Interface/API";
import ButtonTypes from "../../Interface/ButtonTypes";
import DataActions from "../../Interface/DataActions";
import Sorts from "../../Interface/Sorts";
import AppContext from "../../Store";
import Actions from "../../Store/Actions";

const Home: React.FC = () => {
  const [keyword, setKeyword] = useState("");
  const [timeoutIds, setTimeoutIds] = useState<number[]>([]);

  const {
    state: {
      currentPage,
      filteredData,
      data,
      activeItem,
      activeSort,
      pendingActions,
      fetchingData,
    },
    dispatch,
  } = useContext(AppContext);

  const total = filteredData?.length || 0;
  const perPage = import.meta.env.VITE_PER_PAGE || 5;
  const currentData = filteredData.slice(
    (currentPage - 1) * perPage,
    currentPage * perPage
  );

  useEffect(() => {
    // clear all lingering timeouts at component unmount
    return () => {
      timeoutIds.forEach((timeoutId) => {
        clearTimeout(timeoutId);
      });
    };
  }, []);

  const handleOnSubmit = (event: React.MouseEvent<HTMLFormElement>) => {
    event.preventDefault();
    const newFilteredData = data.filter((item) => {
      return (
        item?.name?.toLocaleLowerCase().includes(keyword.toLocaleLowerCase()) ||
        item?.type?.toLocaleLowerCase().includes(keyword.toLocaleLowerCase()) ||
        item?.description
          ?.toLocaleLowerCase()
          .includes(keyword.toLocaleLowerCase())
      );
    });

    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: newFilteredData,
    });
    dispatch({
      type: Actions.CHANGE_CURRENT_ACTIVE_ITEM,
      payload: null,
    });
    dispatch({
      type: Actions.UPDATE_ACTIVE_SORT,
      payload: Sorts.DEFAULT,
    });
  };
  const handleOnKeywordChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setKeyword(event.currentTarget.value);
  };

  const handleDeleteItem =
    (item: APIEndpointType, relativePosition: number) =>
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.stopPropagation();
      if (
        pendingActions.some(
          (action) =>
            action.type === DataActions.DELETE && action.item.id === item.id
        )
      ) {
        return;
      }

      const timeoutId = setTimeout(async () => {
        dispatch({
          type: Actions.PERSIST_PENDING_ACTION,
          payload: null,
        });

        dispatch({
          type: Actions.REMOVE_PENDING_ACTION,
          payload: {
            type: DataActions.DELETE,
            item,
          },
        });
      }, 3000);

      dispatch({
        type: Actions.ADD_PENDING_ACTION,
        payload: {
          type: DataActions.DELETE,
          item: item,
          position: relativePosition + (currentPage - 1) * perPage,
        },
      });

      const newFilteredData = filteredData.flatMap((filteredItem) => {
        return item.id === filteredItem.id ? [] : [filteredItem];
      });
      const newData = data.flatMap((dataItem) => {
        return item.id === dataItem.id ? [] : [dataItem];
      });

      dispatch({
        type: Actions.UPDATE_FILTERED_DATA,
        payload: newFilteredData,
      });

      dispatch({
        type: Actions.UPDATE_DATA,
        payload: newData,
      });

      setTimeoutIds((timeoutIds) => {
        return [...new Set([...timeoutIds, timeoutId])];
      });
    };

  const sortDataAscending = () => {
    const newData = filteredData.sort((item1, item2) =>
      item1.name.toLowerCase() > item2.name.toLowerCase() ? 1 : -1
    );
    dispatch({
      type: Actions.UPDATE_ACTIVE_SORT,
      payload: Sorts.NAME_UP,
    });
    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: newData,
    });
  };

  const sortDataDescending = () => {
    const newData = filteredData.sort((item1, item2) =>
      item1.name.toLowerCase() > item2.name.toLowerCase() ? -1 : 1
    );
    dispatch({
      type: Actions.UPDATE_ACTIVE_SORT,
      payload: Sorts.NAME_DOWN,
    });
    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: newData,
    });
  };

  const handleCreatePopup = () => {
    dispatch({
      type: Actions.CREATE_ITEM_POPUP,
      payload: true,
    });
  };
  return (
    <StyledHome>
      <StyledHeader>
        <div>
          <h1>API Browser</h1>

          <span className="subtitle">
            {fetchingData
              ? "Fetching data ..."
              : `${total} endpoint${total === 1 ? "" : "s"} found`}
          </span>
        </div>
        <StyledRow className="no-background">
          <StyledForm onSubmit={handleOnSubmit}>
            <input
              value={keyword}
              onChange={handleOnKeywordChange}
              className="search-keyword"
              type="text"
              placeholder="Type and hit enter"
            />
          </StyledForm>
          <StyledButton category={ButtonTypes.MAIN} onClick={handleCreatePopup}>
            Create
          </StyledButton>
        </StyledRow>
      </StyledHeader>
      <div>
        <hr color="#eee" />
        <div>
          <DataBrowser
            data={currentData}
            activeItem={activeItem}
            activeSort={activeSort}
            handleDeleteItem={handleDeleteItem}
            sortAscending={sortDataAscending}
            sortDescending={sortDataDescending}
          />
        </div>
      </div>
      <Paginator total={total} current={currentPage} perPage={perPage} />
    </StyledHome>
  );
};

export default Home;
