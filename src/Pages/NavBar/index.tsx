import React from "react";
import StyledColumns from "../../Components/Styled/Home/StyledColumn";
import StyledRow from "../../Components/Styled/Home/StyledRow";
import StyledNavBar from "../../Components/Styled/NavBar";

const NavBar = () => {
  return (
    <StyledNavBar>
      <div className="profile"></div>
      <StyledColumns>
        <h3 className="name">Haileyesus Zemed</h3>
        <h4 className="company">Zettablock</h4>
      </StyledColumns>
    </StyledNavBar>
  );
};

export default NavBar;
