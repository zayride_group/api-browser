import { useEffect, useReducer, useState } from "react";
import Popup from "./Components/Popup";
import SnackBar from "./Components/SnackBar";
import { StyledApp } from "./Components/Styled/App";
import StyledColumns from "./Components/Styled/Home/StyledColumn";
import StyledMask from "./Components/Styled/StyledMask";
import APIEndpointType from "./Interface/API";
import Home from "./Pages/Home";
import NavBar from "./Pages/NavBar";
import AppContext from "./Store";
import Actions from "./Store/Actions";
import InitialState from "./Store/InitialState";

import reducer from "./Store/reducer";

function App() {
  const [state, dispatch] = useReducer(reducer, InitialState);
  

  const { updateItem, pendingActions, createNewItem } = state;

  const updateFilteredData = (data: APIEndpointType[]) => {
    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: data,
    });
  };

  const updateData = (data: APIEndpointType[]) => {
    dispatch({
      type: Actions.UPDATE_DATA,
      payload: data,
    });
  };

  const fetchData = async () => {
    dispatch({
      type: Actions.FETCHING_DATA,
      payload: true,
    })
    const apiURL =
      import.meta.env.API_URL ||
      "https://62a6bb9697b6156bff7e6251.mockapi.io/v1/";
    const data = await (await fetch(`${apiURL}/apis`)).json();

   
    updateData(data);
    updateFilteredData(data);

    dispatch({
      type: Actions.FETCHING_DATA,
      payload: false,
    })
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <StyledApp>
        <NavBar />
        <Home />
      </StyledApp>
      {<StyledMask masked={!!updateItem || createNewItem} />}

      {pendingActions.length ? (
        <StyledColumns className="snackbars">
          {pendingActions.map((pendingAction) => {
            return (
              <SnackBar
                key={pendingAction.item.id}
                pendingAction={pendingAction}
              />
            );
          })}
        </StyledColumns>
      ) : null}

      {updateItem || createNewItem ? <Popup newItem={createNewItem} /> : null}
    </AppContext.Provider>
  );
}

export default App;
