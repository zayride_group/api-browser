enum Sorts {
  NAME_UP = "NAME_UP",
  NAME_DOWN = "NAME_DOWN",
  DEFAULT = "DEFAULT",
}

export default Sorts;
