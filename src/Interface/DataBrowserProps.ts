import React from "react";
import APIEndpointType from "./API";
import Sorts from "./Sorts";

interface DataBrowserProps {
  data: APIEndpointType[];
  activeItem: APIEndpointType | null;
  activeSort: Sorts;
  handleDeleteItem: (
    item: APIEndpointType,
    relativePosition: number
  ) => (event: React.MouseEvent<HTMLButtonElement>) => void;
  sortAscending: () => void;
  sortDescending: () => void;
}

export default DataBrowserProps;
