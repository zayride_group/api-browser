import APIEndpointType from "./API";
import DataActions from "./DataActions";

interface PendingAction {
  type: DataActions;
  item: APIEndpointType;
  newItem?: APIEndpointType;
  position: number;
}
export default PendingAction;
