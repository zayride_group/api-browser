interface PaginatorProps {
  total: number;
  current: number;
  perPage: number;
}

export default PaginatorProps;
