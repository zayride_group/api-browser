interface APIEndpointType {
  id: string;
  name: string;
  type: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}

export default APIEndpointType;
