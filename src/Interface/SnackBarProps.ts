import PendingAction from "./PendingAction";

interface SnackBarProps {
  pendingAction: PendingAction;
}

export default SnackBarProps;
