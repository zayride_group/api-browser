import APIEndpointType from "./API";
import DataActions from "./DataActions";
import PendingAction from "./PendingAction";
import Sorts from "./Sorts";

interface Store {
  currentPage: number;
  activeItem: APIEndpointType | null;
  updateItem: APIEndpointType | null;
  filteredData: APIEndpointType[];
  data: APIEndpointType[];
  activeSort: Sorts;
  pendingActions: PendingAction[];
  createNewItem: boolean;
  fetchingData: boolean;
}

export default Store;
