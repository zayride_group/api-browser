enum ButtonTypes {
  MAIN = "MODIFY",
  SECONDARY = "DELETE",
  ACCENT = "ACCENT",
}

export default ButtonTypes;
