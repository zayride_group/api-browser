import React, { useContext, useEffect } from "react";
import ButtonTypes from "../Interface/ButtonTypes";
import PaginatorProps from "../Interface/PaginatorProps";
import AppContext from "../Store";
import Actions from "../Store/Actions";
import StyledRow from "./Styled/Home/StyledRow";
import StyledButton from "./Styled/StyledButton";

const Paginator: React.FC<PaginatorProps> = ({ total, current, perPage }) => {
  const pagesCount = Math.ceil(total / perPage);
  const { dispatch } = useContext(AppContext);

  const setCurrentPage = (page: number) => () => {
    dispatch({ type: Actions.CHANGE_CURRENT_PAGE, payload: page });
  };

  useEffect(() => {

    if(!total) return
    if (pagesCount < current) {
      dispatch({ type: Actions.CHANGE_CURRENT_PAGE, payload: pagesCount });
    } 
  }, [total, pagesCount, current]);

  return (
    <StyledRow className="no-background paginator">
      {new Array(pagesCount).fill("page").map((_, index) => {
        const page = index + 1;
        const isCurrentPage = current === page;

        return (
          <StyledButton
            category={ButtonTypes.ACCENT}
            isFilled={isCurrentPage}
            isOutlined={!isCurrentPage}
            key={index}
            onClick={setCurrentPage(page)}
            className="small"
          >
            {index + 1}
          </StyledButton>
        );
      })}
    </StyledRow>
  );
};

export default Paginator;
