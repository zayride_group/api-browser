import styled from "styled-components";
interface MaskProps {
  masked: boolean;
}

const StyledMask = styled.div<MaskProps>`
  position: absolute;
  top: 0;
  background: black;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: ${({ masked: mask }) => (mask ? `.8` : `0`)};
  z-index: ${({ masked: mask }) => (mask ? 1 : -1)};
  padding: 0;
  margin: 0;
  transition: opacity 200ms ease-in-out;
  overflow: hidden;
`;
export default StyledMask;
