import styled from "styled-components";

const StyledSnackBar = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  gap: 0.5rem;
  background-color: #555;
  border: 1px solid whitesmoke;
  border-radius: 1rem;
  text-transform: capitalize;
  color: white;
  padding: 0.25rem 1rem;
`;

export default StyledSnackBar;
