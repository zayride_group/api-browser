import styled from "styled-components";

const StyledTable = styled.div`
  font-size: 1.2rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
  margin: 1rem 0;
  .sort {
    font-size: 1rem;
    cursor: pointer;
    &.up {
      margin-left: .5rem ;
    }
    &.down {
      margin-left: .25rem;
    }
    &.active {
      color: #03a9f4
    }

  }
`;
export default StyledTable;
