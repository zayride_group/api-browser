import styled from "styled-components";

const StyledDescription = styled.div`
  transition: 200ms ease-in-out;
  padding: 2rem;
  background-color: #777;
  color: white;
  border-bottom-right-radius: .25rem;
  border-bottom-left-radius: .25rem;
  .title {
    margin: 0 0 0.5rem 0;
    text-transform: capitalize;
    font-size: 1.1rem;
  }
  .entry {
    margin-bottom: 1rem;
  }
  .description {
    font-size: 1rem;
  }
`;
export default StyledDescription;
