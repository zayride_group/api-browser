import styled from "styled-components";

const StyledItem = styled.div`
  background-color: whitesmoke;

  border-radius: 0.4rem;
  cursor: pointer;
`;
export default StyledItem;
