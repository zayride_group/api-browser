import styled from "styled-components";

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  gap: 1rem;

  align-self: flex-end;
  .description-textarea {
    height: 8rem;
    width: 80%;
  }
  .search-keyword {
    padding: 1rem;
    outline-color: #03a9f4;
    align-self: flex-end;
    width: 12rem;
    font-size: 1rem;
    &::placeholder {
      font-size: 1rem;
    }
  }
`;
export default StyledForm;
