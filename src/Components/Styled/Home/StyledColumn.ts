import styled from "styled-components";

const StyledColumns = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-start;
  gap: 1rem;

  &.form {
    input,
    textarea {
      padding: 0.5rem 0.5rem;
      width: 80%;
      margin-bottom: 0.5rem;
      outline-color: #03a9f4;
    }
    textarea {
      height: 8rem;
    }
  }
  &.snackbars {
    position: fixed;
    bottom: 1rem;
    left: 35vw;
    width: max-content;
    text-overflow: ellipsis;
    overflow: scroll;
  }
`;

export default StyledColumns;
