import styled from "styled-components";

const StyledHome = styled.div`
  background-color: white;
  padding: 1rem;
  transition: 200ms ease-in-out;
  hr {
    margin: 1rem 0 2rem 0;
    background-color: whitesmoke;
  }
`;

export { StyledHome };
