import styled from "styled-components";

const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  .close {
    font-weight: bold;
    color: darkgray;
    cursor: pointer;
    text-transform: capitalize;
  }
`;
export default StyledHeader;
