import styled from "styled-components";

interface StyledRowProps {
  rounded?: boolean;
  hasPadding?: boolean;
}

const StyledRow = styled.div<StyledRowProps>`
  display: flex;
  justify-items: flex-start;
  gap: 1rem;
  align-items: stretch;
  background-color: whitesmoke;
  padding: ${({ hasPadding = true }) => (hasPadding ? "1rem 2rem" : "0")};
  border-radius: ${({ rounded }) => (rounded ? ".4rem" : "none")};
  * {
    flex: 1;
  }
  h4 {
    margin: 0;
  }
  &.no-background {
    background-color: transparent;
  }
  .actionButton {
    align-content: stretch;
  }
  &.paginator button {
    flex: 0;
  }
`;
export default StyledRow;
