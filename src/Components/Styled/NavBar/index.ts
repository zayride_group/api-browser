import styled from "styled-components";

const StyledNavBar = styled.div`
  padding: 1rem 3rem;
  background-color: white;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  text-transform: capitalize;

  .name,
  .company {
    margin: 0;
  }

  .profile {
    width: 4rem;
    height: 4rem;
    border-radius: 50%;
    background-color: whitesmoke;
  }
`;

export default StyledNavBar;
