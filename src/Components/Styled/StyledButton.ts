import styled from "styled-components";
import ButtonTypes from "../../Interface/ButtonTypes";

interface ButtonProps {
  category: ButtonTypes;
  isOutlined?: boolean;
  isFilled?: boolean;
}

const ButtonTypeColor = {
  [ButtonTypes.MAIN]: "#03a9f4",
  [ButtonTypes.SECONDARY]: "#d32f2f",
  [ButtonTypes.ACCENT]: "#03a9f4",
} as const;

const ButtonTypeHoverColor = {
  [ButtonTypes.MAIN]: "#007ac1",
  [ButtonTypes.SECONDARY]: "#9a0007",
  [ButtonTypes.ACCENT]: "#007ac1",
}

const StyledButton = styled.button<ButtonProps>`
  color: ${({ isOutlined }) => (isOutlined ? "black" : "white")};
  background-color: ${({ category, isFilled = true }) =>
    isFilled ? ButtonTypeColor[category] : "transparent"};
  border: ${({ isOutlined, category }) =>
    isOutlined ? `1px solid ${ButtonTypeColor[category] || "black"}` : "none"};
  padding: 0.6rem 1rem;
  border-radius: 0.25rem;
  box-shadow: none;
  font-size: 1.05rem;
  min-width: 3rem;
  max-width: 6rem;
  cursor: pointer;
  transition: 100ms ease-in-out;
  :hover {
    background-color: ${({ category, isFilled = true }) =>
      isFilled ? ButtonTypeHoverColor[category] : "transparent"};
  }
  
`;

export default StyledButton;
