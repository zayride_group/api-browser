import styled from "styled-components";

const StyledApp = styled.div`
  padding: 2rem 1rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;

export { StyledApp };
