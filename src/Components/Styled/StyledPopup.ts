import styled from "styled-components";

const StyledPopup = styled.div`
  background-color: white;
  border: 1px solid whitesmoke;
  padding: 2rem 1rem;
  border-radius: 0.4rem;
  position: fixed;
  top: 30vh;
  left: 30vw;
  right: 30vw;
  width: 40vw;
  height: max-content;
  z-index: 1;

  .popup-title {
    margin: 0;
  }
  .popup-description {
    font-size: 1.15rem;
    font-weight: 500;
  }
`;
export default StyledPopup;
