import React, {
  ChangeEvent,
  FormEvent, useContext,
  useEffect,
  useState
} from "react";
import ButtonTypes from "../Interface/ButtonTypes";
import DataActions from "../Interface/DataActions";
import PopupProps from "../Interface/PopupProps";
import AppContext from "../Store";
import Actions from "../Store/Actions";
import StyledColumns from "./Styled/Home/StyledColumn";
import StyledForm from "./Styled/Home/StyledForm";
import StyledHeader from "./Styled/Home/StyledHeader";
import StyledButton from "./Styled/StyledButton";
import StyledPopup from "./Styled/StyledPopup";

const Popup: React.FC<PopupProps> = ({ newItem }) => {
  const {
    state: { updateItem, data, filteredData },
    dispatch,
  } = useContext(AppContext);

  const [newName, setNewName] = useState("");
  const [newType, setNewType] = useState("");
  const [newDescription, setNewDescription] = useState(updateItem?.description);
  const [timeoutIds, setTimeoutIds] = useState<number[]>([]);

  useEffect(() => {
    // clear all lingering timeouts at component unmount
    return () => {
      timeoutIds.forEach((timeoutId) => {
        clearTimeout(timeoutId);
      });
    };
  }, []);

  const handleNewName = (event: ChangeEvent<HTMLInputElement>) => {
    setNewName(event.currentTarget.value);
  };
  const handleNewType = (event: ChangeEvent<HTMLInputElement>) => {
    setNewType(event.currentTarget.value);
  };
  const handleNewDescription = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setNewDescription(event.currentTarget.value);
  };

  const handleUpdateItem = (event: FormEvent<HTMLFormElement>) => {
    event.stopPropagation();
    event.preventDefault();

    const newItem = { ...updateItem, description: newDescription };
    const newData = data.map((item) => {
      return item.id === updateItem?.id ? newItem : item;
    });

    const newFilteredData = filteredData.map((item) => {
      return item.id === updateItem?.id ? newItem : item;
    });

    dispatch({
      type: Actions.UPDATE_DATA,
      payload: newData,
    });

    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: newFilteredData,
    });

    const timeoutId = setTimeout(() => {
      dispatch({
        type: Actions.PERSIST_PENDING_ACTION,
        payload: null,
      });
      dispatch({
        type: Actions.REMOVE_PENDING_ACTION,
        payload: { item: updateItem },
      });
    }, 3000);

    setTimeoutIds((currentTimeoutIds) => [
      ...new Set([...currentTimeoutIds, timeoutId]),
    ]);
    dispatch({
      type: Actions.CHANGE_CURRENT_UPDATE_ITEM,
      payload: null,
    });
    dispatch({
      type: Actions.CREATE_ITEM_POPUP,
      payload: false,
    });

    dispatch({
      type: Actions.ADD_PENDING_ACTION,
      payload: {
        type: DataActions.UPDATE,
        item: updateItem,
        newItem,
      },
    });
  };

  const handleNewItem = async (event: FormEvent<HTMLFormElement>) => {
    event.stopPropagation();
    event.preventDefault();

    setTimeout(() => {
      dispatch({
        type: Actions.PERSIST_PENDING_ACTION,
        payload: null,
      });
      dispatch({
        type: Actions.REMOVE_PENDING_ACTION,
        payload: { item: newItem },
      });
    }, 3000);

    const createdItem = await (
      await fetch(
        `${
          import.meta.env.API_URL ||
          "https://62a6bb9697b6156bff7e6251.mockapi.io/v1"
        }/apis`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: newName,
            type: newType,
            description: newDescription,
          }),
        }
      )
    ).json();

    dispatch({
      type: Actions.CREATE_ITEM_POPUP,
      payload: false,
    });
    dispatch({
      type: Actions.UPDATE_DATA,
      payload: [...data, createdItem],
    });
    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: [...filteredData, createdItem],
    });
  };

  const handleClosePopup = () => {
    dispatch({ type: Actions.CHANGE_CURRENT_UPDATE_ITEM, payload: null });
    dispatch({
      type: Actions.CREATE_ITEM_POPUP,
      payload: false,
    });
  };

  return (
    <StyledPopup>
      <StyledHeader>
        <h3 className="popup-title">
          {newItem ? "Create a New Item" : `Update "${updateItem?.name}"`}
        </h3>
        <span className="close" onClick={handleClosePopup}>
          ✕
        </span>
      </StyledHeader>
      <hr color="#eee" />
      <StyledForm
        as="form"
        onSubmit={newItem ? handleNewItem : handleUpdateItem}
      >
        {newItem && (
          <StyledColumns className="form">
            <label htmlFor="name"> Name</label>
            <input
              id="name"
              type="text"
              value={newName}
              onChange={handleNewName}
            />

            <label htmlFor="type"> Type</label>
            <input
              id="type"
              type="text"
              value={newType}
              onChange={handleNewType}
            />

            <label htmlFor="description"> Description</label>
            <textarea
              id="description"
              value={newDescription}
              onChange={handleNewDescription}
            />

            <StyledButton category={ButtonTypes.MAIN}>Create</StyledButton>
          </StyledColumns>
        )}
        {!newItem && (
          <>
            <span className="popup-description">Description</span>
            <textarea
              className="description-textarea"
              value={newDescription}
              onChange={handleNewDescription}
            ></textarea>
            <StyledButton category={ButtonTypes.ACCENT}>Update</StyledButton>{" "}
          </>
        )}
      </StyledForm>
    </StyledPopup>
  );
};

export default Popup;
