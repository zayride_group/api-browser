import React, { useContext } from "react";
import APIEndpointType from "../Interface/API";
import ButtonTypes from "../Interface/ButtonTypes";
import DataActions from "../Interface/DataActions";
import SnackBarProps from "../Interface/SnackBarProps";
import AppContext from "../Store";
import Actions from "../Store/Actions";
import StyledButton from "./Styled/StyledButton";
import StyledSnackBar from "./Styled/StyledSnackBar";

const SnackBar: React.FC<SnackBarProps> = ({ pendingAction }) => {
  const {
    state: { pendingActions, data, filteredData, activeSort },
    dispatch,
  } = useContext(AppContext);

  const undoChange = () => {
    dispatch({
      type: Actions.REMOVE_PENDING_ACTION,
      payload: pendingAction,
    });

    let newData: APIEndpointType[] = [];
    let newFilteredData: APIEndpointType[] = [];
    const { item, position } = pendingAction;

    if (pendingAction.type === DataActions.DELETE) {
      newData = [...data.slice(0, position), item, ...data.slice(position)];
      newFilteredData = [
        ...filteredData.slice(0, position),
        item,
        ...data.slice(position),
      ];
    } else if (pendingAction.type === DataActions.UPDATE) {
      newData = data.map((dataItem) => {
        return dataItem.id === item.id ? item : dataItem;
      });
      newFilteredData = filteredData.map((dataItem) => {
        return dataItem.id === item.id ? item : dataItem;
      });
    }
    dispatch({
      type: Actions.UPDATE_DATA,
      payload: newData,
    });

    dispatch({
      type: Actions.UPDATE_FILTERED_DATA,
      payload: newFilteredData,
    });
  };

  return (
    <StyledSnackBar>
      <StyledButton category={ButtonTypes.SECONDARY} onClick={undoChange}>
        Undo
      </StyledButton>
      <h4>
        {pendingAction.type === DataActions.DELETE ? "Deleting" : "Updating"}{" "}
        {pendingAction.item.name}
      </h4>
    </StyledSnackBar>
  );
};

export default SnackBar;
