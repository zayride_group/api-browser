import React, { useContext } from "react";
import APIEndpointType from "../Interface/API";
import ButtonTypes from "../Interface/ButtonTypes";
import DataBrowserProps from "../Interface/DataBrowserProps";
import Sorts from "../Interface/Sorts";
import AppContext from "../Store";
import Actions from "../Store/Actions";
import StyledDescription from "./Styled/Home/StyledDescription";
import StyledItem from "./Styled/Home/StyledItem";
import StyledRow from "./Styled/Home/StyledRow";
import StyledTable from "./Styled/Home/StyledTable";
import StyledButton from "./Styled/StyledButton";

const DataBrowser: React.FC<DataBrowserProps> = ({
  data,
  activeItem,
  activeSort,
  handleDeleteItem,
  sortAscending,
  sortDescending,
}) => {
  const {
    state: { data: allData },
    dispatch,
  } = useContext(AppContext);

  const setActiveItem = (item: APIEndpointType) => () => {
    let newActiveItem: APIEndpointType | null = item;
    if (activeItem?.id === item.id) {
      newActiveItem = null;
    }
    dispatch({
      type: Actions.CHANGE_CURRENT_ACTIVE_ITEM,
      payload: newActiveItem,
    });
  };

  const setUpdateItem =
    (item: APIEndpointType) => (event: React.MouseEvent<HTMLButtonElement>) => {
      event.stopPropagation();
      dispatch({ type: Actions.CHANGE_CURRENT_UPDATE_ITEM, payload: item });
    };

  return (
    <StyledTable>
      <StyledRow rounded={true}>
        <h4>ID </h4>
        <h4>
          Name
          <span
            className={`sort up ${
              activeSort === Sorts.NAME_UP ? "active" : ""
            }`}
            onClick={sortAscending}
          >
            ↑
          </span>
          <span
            className={`sort down ${
              activeSort === Sorts.NAME_DOWN ? "active" : ""
            }`}
            onClick={sortDescending}
          >
            ↓
          </span>
        </h4>
        <h4>Type</h4>
        <h4>Actions</h4>
      </StyledRow>

      {data.map((item, index) => {
        return (
          <StyledItem key={item.id} onClick={setActiveItem(item)}>
            <StyledRow key={item.id}>
              <span>{item.id}</span>
              <span>{item.name}</span>
              <span>{item.type}</span>
              <StyledRow hasPadding={false}>
                <StyledButton
                  category={ButtonTypes.MAIN}
                  onClick={setUpdateItem(item)}
                >
                  Update
                </StyledButton>
                <StyledButton
                  category={ButtonTypes.SECONDARY}
                  onClick={handleDeleteItem(item, index)}
                >
                  Delete
                </StyledButton>
              </StyledRow>
            </StyledRow>
            {activeItem?.id === item.id && (
              <StyledDescription>
                {Object.entries(item).map((itemEntry, index) => {
                  return (
                    <div key={index} className="entry">
                      <h4 className="title">{itemEntry[0]}</h4>
                      <span className="description">
                        {JSON.stringify(itemEntry[1]).replace(/\"/g, "")}
                      </span>
                    </div>
                  );
                })}
              </StyledDescription>
            )}
          </StyledItem>
        );
      })}
    </StyledTable>
  );
};

export default DataBrowser;
